import asyncio
import time

from PandaX_Userbot import *
from PandaX_Userbot.Panda import *
from PandaX_Userbot.functions.all import *
from PandaX_Userbot.functions.sudos import *
from PandaX_Userbot.version import PandaX_version
from PandaX_Userbot.functions.nsfw_db import *
from PandaX_Userbot.functions.asstcmd_db import *
from PandaX_Userbot.functions.broadcast_db import *
from PandaX_Userbot.functions.gban_mute_db import *
from telethon import Button
from telethon.tl import functions, types
from PandaX_Userbot.utils import *
from strings import get_string


try:
    import glitch_me
except ModuleNotFoundError:
    os.system(
        "git clone https://github.com/1Danish-00/glitch_me.git && pip install -e ./glitch_me"
    )


start_time = time.time()
petercordpanda_bot_version = "_PandaX_Userbot_"

OWNER_NAME = petercordpanda_bot.me.first_name
OWNER_ID = petercordpanda_bot.me.id

List = []
Dict = {}
N = 0

NOSPAM_CHAT = [
    -1001387666944,  # @PyrogramChat
    -1001109500936,  # @TelethonChat
    -1001050982793,  # @Python
    -1001256902287,  # @DurovsChat
]

KANGING_STR = [
    "Using Witchery to kang this sticker...",
    "Plagiarising hehe...",
    "Inviting this sticker over to my pack...",
    "Kanging this sticker...",
    "Hey that's a nice sticker!\nMind if I kang?!..",
    "Hehe me stel ur stiker...",
    "Ay look over there (☉｡☉)!→\nWhile I kang this...",
    "Roses are red violets are blue, kanging this sticker so my pack looks cool",
    "Imprisoning this sticker...",
    "Mr.Steal-Your-Sticker is stealing this sticker... ",
]
