"""
💐 Commands Available -
• `{i}pong`
   ketik {i}pong untuk melihat kecepatan Panda Userbotmu.
"""

import asyncio
from datetime import datetime

from . import *

@ilhammansiz_cmd(pattern="ping")
async def dsb(ult):
    start = datetime.now()
    await ult.edit("`♻♻♻♻♻♻♻....`")
    await asyncio.sleep(0.5)
    await ult.edit("`⏰⏰⏰⏰⏰⏰⏰....⏰⏰⏰⏰⏰`")
    await asyncio.sleep(0.5)
    await ult.edit("`⏳⏳⏳⏳⏳⏳⏳⏳⏳⏳.........⏳⏳⏳⏳⏳⏳`")
    await asyncio.sleep(0.5)
    await ult.edit("`♨`")
    await asyncio.sleep(0.5)
    await ult.edit("`☠`")
    await asyncio.sleep(0.5)
    await ult.edit("`💪`")
    await asyncio.sleep(0.5)
    end = datetime.now()
    ms = (end - start).microseconds / 1000
    await ult.edit("┏━《 **𝗣 𝗔 𝗡 𝗗 𝗔** 》\n┣➠  __𝐏𝐢𝐧𝐠:__  `── 1000 milliseconds`\n┗➠ 𝗣 𝗔 𝗡 𝗗 𝗔 𝗨𝗦𝗘𝗥𝗕𝗢𝗧")

